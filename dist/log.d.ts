declare type TLogLevel = 'none' | 'error' | 'warn' | 'info';
export declare const shouldLog: (level?: TLogLevel) => boolean;
export declare const logger: (level: TLogLevel, levelSymbol: string, fn?: Function) => (...a: any[]) => void;
export declare const log: {
    info: (...a: any[]) => void;
    success: (...a: any[]) => void;
    warn: (...a: any[]) => void;
    error: (...a: any[]) => void;
};
export {};

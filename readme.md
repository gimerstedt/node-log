# log
simple log util, env-var LOG\_LEVEL controls log level (none, error, warn, info)

## example logging
``` typescript
import { log } from '@gimerstedt/log'

log.info('yup')
log.success('yup')
log.warn('yup')
log.error('yup')
```

import chalk from 'chalk'

type TLogLevel = 'none' | 'error' | 'warn' | 'info'

export const shouldLog = (level: TLogLevel = 'info'): boolean => {
  const env = process.env.LOG_LEVEL ? process.env.LOG_LEVEL : 'info'
  if (env === 'none') return false
  if (env === 'warn' && level === 'info') return false
  if (env === 'error' && level === 'info') return false
  if (env === 'error' && level === 'warn') return false
  return true
}

const pad27 = (s: string) => {
  let ret = s
  while (ret.length < 7) ret += ' '
  return ret
}

export const logger = (
  level: TLogLevel,
  levelSymbol: string,
  fn: Function = console.log,
) => (...a: any[]) => {
  if (!shouldLog(level)) return
  fn(`${new Date().toISOString()} ${levelSymbol}`, ...a)
}

const info = logger('info', chalk.blue(pad27('info')))
const success = logger('info', chalk.green(pad27('success')))
const warn = logger('warn', chalk.yellow(pad27('warning')))
const error = logger('error', chalk.red(pad27('error  ')))

export const log = {
  info,
  success,
  warn,
  error,
}

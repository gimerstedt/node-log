import * as sut from './log'

describe('shouldLog', () => {
  beforeAll(() => {
    delete process.env.LOG_LEVEL
  })

  it('works', () => {
    expect(sut.shouldLog('info')).toBeTruthy()
    expect(sut.shouldLog('warn')).toBeTruthy()
    expect(sut.shouldLog('error')).toBeTruthy()

    process.env.LOG_LEVEL = 'info'

    expect(sut.shouldLog('info')).toBeTruthy()
    expect(sut.shouldLog('warn')).toBeTruthy()
    expect(sut.shouldLog('error')).toBeTruthy()

    process.env.LOG_LEVEL = 'warn'

    expect(sut.shouldLog('info')).toBeFalsy()
    expect(sut.shouldLog('warn')).toBeTruthy()
    expect(sut.shouldLog('error')).toBeTruthy()

    process.env.LOG_LEVEL = 'error'

    expect(sut.shouldLog('info')).toBeFalsy()
    expect(sut.shouldLog('warn')).toBeFalsy()
    expect(sut.shouldLog('error')).toBeTruthy()

    process.env.LOG_LEVEL = 'none'

    expect(sut.shouldLog('info')).toBeFalsy()
    expect(sut.shouldLog('warn')).toBeFalsy()
    expect(sut.shouldLog('error')).toBeFalsy()
  })
})

describe('logger', () => {
  beforeAll(() => {
    delete process.env.LOG_LEVEL
  })

  it('works', () => {
    const fn = jest.fn()
    let calls = 0

    const infoLogger = sut.logger('info', 'prefix', fn)
    infoLogger('text')

    const call1 = fn.mock.calls[calls++]
    expect(call1[0]).toContain('prefix')
    expect(call1[1]).toContain('text')

    const warnLogger = sut.logger('warn', 'asdf', fn)
    warnLogger('123')

    const call2 = fn.mock.calls[calls++]
    expect(call2[0]).toContain('asdf')
    expect(call2[1]).toContain('123')

    const errorLogger = sut.logger('error', 'qwer', fn)
    errorLogger('zxcv')

    const call3 = fn.mock.calls[calls++]
    expect(call3[0]).toContain('qwer')
    expect(call3[1]).toContain('zxcv')

    const before1 = calls

    process.env.LOG_LEVEL = 'error'
    infoLogger('syns inte')

    expect(fn.mock.calls.length).toEqual(before1)
  })
})

#!/usr/bin/env node
const { log } = require('./dist')

log.success('success')
log.info('info')
log.warn('warn')
log.error('error')

log.success({ a: 1 }, [3, 1])
log.info([123])
log.warn([{ b: 2 }])
log.error(JSON.parse)
